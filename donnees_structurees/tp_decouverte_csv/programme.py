import csv

def afficher_descripteurs ():
    """ Cette fonction affiche la première ligne du fichier csv """
    with open('centres-vaccination.csv', 'r', encoding='utf8') as f:
        for champs in f.readline().strip().split(';'):
            print(champs)

def compter_les_colonnes ():
    """ Cette fonction compte le nombre d’entrées renseignées pour chaque colonne """
    compte = {}
    with open('centres-vaccination.csv', 'r', encoding='utf8') as f:
        title = f.readline().strip().split(';')
        for ligne in f.readlines():
            for (index, element) in zip(title,ligne.strip().split(';')):
                if index not in compte:
                    compte[index] = []
                if element.strip() != '':
                    compte[index].append(element)
    return compte

def afficher_compte_colonnes ():
    """ Cette fonction affiche le nombre d’élément d’une colonne """
    colonnes = compter_les_colonnes()
    for index in colonnes:
        print(index, ' : ', len(colonnes[index]))

def afficher_contenu_colonne (colonne):
    """ Cette fonction affiche les différentes valeurs d’une colonne """
    colonnes = compter_les_colonnes()
    resultat = {}
    for element in colonnes[colonne]:
        if element not in resultat:
            resultat[element] = 0
        resultat[element] += 1
    for element in resultat:
        print(element, ' : ', resultat[element])


# ------------------------------------------------------------------------------------------------------
#                       Écrivez votre code en dessous de cette ligne
# ------------------------------------------------------------------------------------------------------


