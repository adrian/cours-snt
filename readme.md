# Cours de seconde SNT
Adrian Amaglio

## Licence
cc-by-sa

## Contenu réutilisable
### SNT
- [Cartographie — Dijkstra](./cartographie/dijkstra/cours.pdf) Dérouler l’algorithme de Dijkstra pour en saisir le principe.
- [Données structurées – Nextcloud](./donnees_structurees/tp_nextcloud/sujet.pdf) Apprendre à se servir d’un drive, créer, partager, gérer des fichiers.
- [Photographie numérique – Lire un fichier image](./photo\ numérique/activité_décryptage_image/sujet.pdf) Comprendre comment une image est décrite dans un fichier.
- [Python – Introduction](./python/tp_intro_turtle/sujet.pdf) Diriger la tortue pour s’initier au Python.
- [Trier et critiquer l’information](./réseaux_sociaux/comment_sinformer/sujet.pdf) Comment bien s’informer avec les technologies actuelles.
- [Réseaux sociaux — Graphes](./réseaux_sociaux/graphes/cours.pdf) Initiation aux graphes.

## Comment compiler les documents latex
Les documents latex doivent être dans un dossier qui se trouve à la racine du répertoire : `cours-snt/internet`.
Sans quoi l’ajout de licence ne fonctionnera pas.  
Utilisez votre environnement latex habituel ou les commandes suivantes :
```
cd photo\ numérique/activité_décryptage_image/
pdflatex sujet.tex
```

## Contributions
Les propositions, améliorations et autres PR sont les bienvenues :)
