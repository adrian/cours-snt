# Définitions sur le thème : Internet
**Internet** est l’interconnexion de nombreux réseaux de tailles et natures variées. C’est un réseau mondial de communications numériques qui sert de support à de nombreuses ressources et services comme le web, les mails ou la téléphonie.
Tous les appareils y sont connectés grâce aux protocoles internet : IP (et TCP).

**Réseau** : Ensemble de machines capables de communiquer entre elles.

**3G, 4G, 5G** : Protocoles de communication sans fil ayant une grande porté. Ils sont utilisés par des appareils très mobiles (téléphones, clés 3G…).

**Wifi** : Protocole de communication sans fil de faible porté (30 mètres maximum), il est utilisé pour créer des réseaux domestiques ou de plus grandes organisations.

**Ethernet** : Protocole de communication filaire utilisé pour relier deux appareils entre eux. Il est utilisé sur des câbles en cuivre.

**ADSL** : Protocole de communication filaire utilisé pour relier une box à son opérateur. Il est utilisé sur des câbles en cuivre.

**Fibre** : Technologie de communication filaire utilisée pour relier deux appareils entre eux. Il s’agit d’un long brin de plastique ou de verre qui guide un rayon lumineux porteur d’informations.

**Fournisseur d’accès à internet (FAI) ou opérateur** : Organisation capable de connecter un tiers au réseau internet.

**Neutralité d’un réseau** : Principe devant garantir l’égalité de traitement de tous les flux de données. Un réseau neutre ne peut pas privilégier certaines communications au détriment d’autres.

**Routeur** : Appareil servant à aiguiller les données à travers les réseaux auxquels elle est connectée. Cette action est appelée routage.

**Serveur** : Appareil proposant un service numérique.

**Centre de calcul, centre de données, ou datacenter** : Bâtiment servant à héberger, sécuriser et climatiser des appareils (serveurs, routeurs…).

**Machine ou appareil** : Ordinateur souvent spécialisé dans une tâche.

