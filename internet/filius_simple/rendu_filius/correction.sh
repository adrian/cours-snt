
tmp="$(mktemp)"

. venv/bin/activate

function evaluate () {
	t="$(mktemp)"
	echo "# TP réseau sur filius"
	echo "# Correction $1"
	unzip -p "$1" projekt/konfiguration.xml > "$t"
	python3 correction.py $t
	rm $t
}


# If first arg is a file, then show its details
# Else processe all dir
if [ -f "$1" ] && [ "$1" != "details" ] ; then
    file="$1"
    evaluate "$file" detail
elif [ -d "$1" ] && [ "$1" != "details" ] ; then
    d="$1"
    for f in "$d"/* ; do
        #evaluate "$f" detail
        evaluate "$f" "$1"
    done

else
    for d in * ; do
        if [ ! -d "$d" ] ; then continue ; fi
        if [[ "$d" = res_* ]] || [ "$d" = venv ] ; then continue ; fi
        echo === $d
        resd="res_$d"
        mkdir -p "$resd"
        rm "$resd"/*

        for f in "$d"/*.fls ; do
			echo "$f"
            evaluate "$f" details > "tmp"
            pandoc -f markdown "tmp" -o "$resd/$(basename "${f%.fls}").pdf"
        done
    done
fi

rm "$tmp"
