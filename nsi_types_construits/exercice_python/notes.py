# Voici un dictionnaire dont la clé est un nom, la valeur une liste de notes
notes = {'John' : [10, 8, 14], 'Amaglio': [2, 8, 0.5], 'Aya': [18, 20, 25]}

# Exercice 1
# Affichez le nom et les notes de chaque personne répertoriée dans le dictionnaire notes.

# Exercice 2
# Pour chaque personne, calculez sa moyenne. Vous la stockerez dans un nouveau dictionnaire que vous appellerez moyenne.
# Affichez ensuite les noms et moyennes de chaque personne.

# Exercice 3
# Créez un dictionnaire nommé classes dans lequel les clés seront des noms de classe (par exemple seconde18) et les valeurs seront les noms des personnes de cette classe.
# Vous pourrez attribuer les personnes du dictionnaire à des classes, il faut au moins une classe qui ait plusieurs personnes.

# Exercice 4
# Créez un dictionnaire nommé moyennes dans lequel les clés seront les noms de classe et les valeurs seront les moyennes de cette classe
