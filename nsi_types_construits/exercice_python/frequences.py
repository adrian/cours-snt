# Exercice 1
# Modifiez la boucle présente à la fin du fichier pour qu’elle remplisse le dictionnaire frequences.
# Pour ce faire, il faut ajouter 1 à la valeur corresponant à une lettre dès qu’on la rencontre lors du parcours du texte
# Certaines valeurs vont poser problème à notre dictionnaire, tester bien que seules les lettres sont utilisées comme clé.

# Exercice 2
# Affichez les valeurs des lettres eiatsr divisées par le nombre total de lettres dans le texte. Vous obtenez un ratio qui sera grand pour les lettres très utilisées.
# Comparez ce ratio avec celui que l’on attribue souvent au français (recherez le sur internet).



# Texte issu de wikipédia
# https://fr.wikipedia.org/wiki/Journée_du_souvenir_trans
texte = """
la journée du souvenir trans, déclinaison française du transgender day of remembrance (tdor), a lieu le 20 novembre dans le monde entier, pour commémorer les personnes trans assassinées pour motif transphobe, c’est-à-dire la haine des personnes trans, et pour attirer l'attention sur les violences subies par les communautés trans4.
la journée du souvenir trans a plusieurs objectifs :
    commémorer toutes les personnes ayant été victimes de crimes haineux et de préjugés,
    sensibiliser sur les crimes haineux envers la communauté trans,
    rendre hommage aux personnes décédées et à leurs proches.
si cette journée marque la solidarité de la communauté lgbt à l’égard des victimes et témoigne de son indignation, elle veut aussi rappeler que la transphobie n’est pas reconnue comme discrimination ou circonstance aggravante pour les crimes par l’état.
il en résulte qu’aucune statistique officielle n’est disponible. en 2011, 221 cas de personnes trans assassinées furent comptabilisés par les associations trans.
ce nombre ne représente pas la réalité puisque le comptage est effectué localement par un nombre réduit d’organisations et dans peu de pays. de plus, la plupart des crimes restent ignorés puisque le caractère transphobe n’est pas retenu par les autorités.
"""

# Ce dictionnaire contiendra en clé les lettres de l’alphabet, en valeur le nombre de fois qu’elles sont apparues dans le texte.
frequences = {'a':0, 'b':0, 'c':0, 'd':0, 'e':0, 'f':0, 'g':0, 'h':0, 'i':0, 'j':0, 'k':0, 'l':0, 'm':0, 'n':0, 'o':0, 'p':0, 'q':0, 'r':0, 's':0, 't':0, 'u':0, 'v':0, 'w':0, 'x':0, 'y':0, 'z':0}

# Cette boucle permet de parcourir le texte, lettre par lettre. À chaque tour de boucle la variable lettre contiendra toutes les lettres du texte, une à une, dans l’ordre.
for lettre in texte:
    # On affiche le texte lettre par lettre.
    print(lettre, end='')

print(frequences)
