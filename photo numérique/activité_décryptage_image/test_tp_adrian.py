# -*- coding: cp1252 -*-
import subprocess
import os
import re

couleurs = {"K": "black","R": "red","G": "green","B": "blue","W": "white"}

text = """
\newcommand{\picI}{\K\B\B\B\K\W\K\G\K\W\\\K\W\G\W\K\W\K\G\K\W\\\K\R\R\R\K} % Un I
% La seconde est sa rotation
\newcommand{\picIrot}{180�}
\newcommand{\picL}{\B\R\G\G\G\B\K\G\G\G\\\B\R\G\G\G\B\K\G\G\G\\\B\R\K\R\G} % Un L
\newcommand{\picLrot}{0�}
\newcommand{\picO}{\K\B\G\R\K\K\W\W\W\K\\\W\B\G\R\W\K\W\W\W\K\\\K\B\G\R\K} % Un O
\newcommand{\picOrot}{90�}
\newcommand{\picV}{\B\G\B\G\B\R\G\R\G\R\\\K\G\K\G\K\R\G\R\G\R\\\B\B\G\B\B} % Un V
\newcommand{\picVrot}{0�}
\newcommand{\picE}{\K\K\K\K\K\W\W\W\W\W\\\W\R\W\R\W\W\R\R\R\W\\\R\R\B\R\R} % Un E
\newcommand{\picErot}{90�}
\newcommand{\picSecond}{\W\W\W\B\W\B\K\W\K\B\\\W\W\W\B\K\W\K\B\K\B\\\W\W\W\B\K} % Un 2
\newcommand{\picSecondrot}{0�}
\newcommand{\picHeart}{\K\R\K\R\K\R\G\R\G\R\\\R\G\W\G\R\K\R\G\R\K\\\K\K\R\K\K} % Un c�ur
\newcommand{\picHeartrot}{0�}
\newcommand{\picExclamation}{\R\K\G\B\W\R\K\G\B\W\\\R\K\G\B\W\R\R\G\W\W\\\R\K\G\B\W} % Deux points d�exclamation
\newcommand{\picExclamationrot}{0�}

\newcommand{\picFirst}{\K\B\B\B\R\K\K\B\R\R\\\W\W\B\W\W\R\R\B\B\K\\\R\R\B\K\K} % Un 1
\newcommand{\picFirstrot}{180�}
\newcommand{\picFour}{\K\W\K\K\K\K\B\K\K\K\\\K\G\G\G\K\K\R\K\R\K\\\K\W\K\W\K} % Un 4
\newcommand{\picFourrot}{180�}
\newcommand{\picSixth}{\G\B\B\B\G\W\B\W\W\W\\\G\B\B\B\G\W\B\W\B\W\\\G\B\B\B\G} % Un 6
\newcommand{\picSixthrot}{0�}
\newcommand{\picSeven}{\R\R\K\K\K\B\G\B\G\B\\\G\R\K\K\K\B\R\K\K\K\\\R\R\K\K\K} % Un 7
\newcommand{\picSevenrot}{-90�}
\newcommand{\picEight}{\G\B\W\B\G\K\K\K\K\K\\\K\R\K\R\K\K\K\K\K\K\\\G\B\W\B\G} % Un 8
\newcommand{\picEightrot}{90�}
\newcommand{\picFourteen}{\R\R\R\R\R\K\W\K\W\K\\\W\K\G\G\G\K\W\G\B\B\\\G\G\G\G\G} % Un 14
\newcommand{\picFourteenrot}{90�}
\newcommand{\picFiveteen}{\G\R\G\G\G\G\R\G\W\G\\\G\G\G\W\G\R\R\R\W\W\\\B\B\B\B\B} % Un 15
\newcommand{\picFiveteenrot}{-90�}
\newcommand{\picSeventeen}{\K\G\K\K\K\K\W\G\W\K\\\K\G\G\G\K\K\W\G\W\K\\\K\G\G\G\K} % Un 17
\newcommand{\picSeventeenrot}{0�}
\newcommand{\picEighteen}{\R\R\R\R\R\K\W\K\W\K\\\G\G\G\G\G\G\B\G\B\G\\\G\G\G\G\G} % Un 18
\newcommand{\picEighteenrot}{90�}
"""

dir_path = os.path.dirname(os.path.realpath(__file__))
#firefox_path = 'D:/Program Files/Mozilla Firefox/firefox.exe'
firefox_path = 'firefox'

def openFirefoxWithHtml(html) :
    if os.path.exists('tmp.html'):
        os.remove('tmp.html')
    with open('tmp.html', 'w', encoding = 'utf8') as f:
        f.write(html)
    subprocess.run([firefox_path, 'file://'+dir_path+'/tmp.html'])

def imageInTab(image):
    string = '<table style="border-collapse: collapse; border: 1px solid black; display: inline-block; margin: 50px">'
    for i in range(0,5) :
        string += '<tr>'
        for j in range(0,5) :
            string += getPixelImage(image[i*5+j])
        string += '</tr>'
    string += '</table>'
    return string

def getPixelImage(pixel):
    return '<td style="width: 50px; height:50px; background-color:' + couleurs[pixel] + '"></td>'

def test(tab):
    string = ''
    for image in tab:
        string += imageInTab(image)
    openFirefoxWithHtml(string)

def text_to_list(text):
    res = []
    p = re.compile('(\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW]\\\\[RGBKW])')
    for line in text.split('\n'):
        found = p.search(line)
        if not found:
            continue
        res.append(list(found.group(0).replace('\\', '')))
    return res


test(text_to_list(text))

