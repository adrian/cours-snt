import turtle as t

# 30 12
# 36 5
# 20 9
# 35 cest joli
# 21 ça va loin
def etoile(nb):
    l = 200
    while True:
        t.forward(l)
        t.left(360/nb+180)
        l *= 1.05

def spirale():
    angle = 5
    nb_tours = 0
    while True:
        nb_tours += 1
        t.forward(1)
        t.left(angle)
        angle -= 0.5/nb_tours

def polygon(l, nb):
    for i in range(nb):
        t.forward(l)
        t.left(360/nb)

def rotating_shape(shape_function, a):
    while True:
        shape_function()
        t.left(a)


if __name__ == '__main__':
    rotating_shape(lambda:polygon(50,4), 20)
    input()
