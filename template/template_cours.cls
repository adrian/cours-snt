% This is a Latex template, inspired from
% http://tutex.tug.org/pracjourn/2005-4/hefferon/hefferon.pdf
%
% Copyright © 2019 Adrian Amaglio <adrian@amaglio.fr>
% This work is free. You can redistribute it and/or modify it under the
% terms of the Do What The Fuck You Want To Public License, Version 2,
% as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.

% Contributors : Marie D,

% -----------------------------------------------------------------------------
%                     Define Document properties
% -----------------------------------------------------------------------------

% --- Class structure: identification part
% ---
\ProvidesClass{../../template/template_cours}[Cours en lycée]
\NeedsTeXFormat{LaTeX2e}


% --- Class structure: declaration of options part
% ---
% This class extends the article class
% Read all the documentclass options; pass them to article,
% ---
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}


% --- Class structure: execution of options part
% ---
\ProcessOptions \relax


% --- Class structure: Loading parent class
% ---
\LoadClass{article}


% --- Force A4 paper
% ---
% TODO: Can we create a A3 or A5 document with this template?
\usepackage[a4paper]{geometry}


% --- Loading common packages
% ---


% --- Be french and utf-8
% ---
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
%\usepackage{amsmath}%why
%\usepackage{amsfonts}%why
%\usepackage{amssymb}%why


% --- Define hyperlinks style
% ---
\usepackage{xcolor}
\usepackage{hyperref}
\hypersetup{pdfpagemode={UseOutlines},
bookmarksopen=true,
bookmarksopenlevel=0,
hypertexnames=false,
colorlinks=true, % Set to false to disable coloring links
citecolor=green!40!black!90, % The color of citations
linkcolor=red!40!black!80, % The color of references to document elements (sections, figures, etc)
urlcolor=blue!40!black!80, % The color of hyperlinks (URLs)
pdfstartview={FitV},
breaklinks=true,
}


% --- Define document margins
% ---
\geometry{top=2cm, bottom=2cm, left=2cm, right=2cm}
%\setlength{\textheight}{715pt} \addtolength{\voffset}{-90pt}
%\setlength{\textwidth}{485pt} \addtolength{\hoffset}{-70pt}


% --- Paper and page layout
% ---
\setlength{\parindent}{0pt}
%\setlength{\headheight}{20pt}
\setlength{\headheight}{14.2pt}


% --- New page for each section
% ---
\usepackage{titlesec}
%\newcommand{\sectionbreak}{\pagebreak}
%not working as expected :(


% --- Define document font
% ---
\usepackage{mathpazo}
% TODO dyslexic friendly font
%\usepackage{newcent}
%Dyslexie
%OpenDyslexic (open-dyslexic) by Abelardo Gonzalez is licensed under a Creative Commons Attribution 3.0 Unported License
%Lexia Readable

% -----------------------------------------------------------------------------
%                     Include and define nice functions 
% -----------------------------------------------------------------------------

% --- Include pictures
% ---
\usepackage{graphicx}
% \usepackage{rotating, graphicx}
% Why
%\usepackage{array}


% --- To display typewriter style text
% ---
\usepackage{listings}


% --- Some useful commands
\newcommand{\bigO}{\textit{O}}
\newcommand\ignore[1]{{}}
\newcommand{\correc}[1]{\textbf{\textcolor{codegreen}{#1}}}
\newcommand{\motcle}[1]{\texttt{\textcolor{blue}{#1}}}
\newcommand{\mothtml}[1]{\texttt{<\textcolor{blue}{#1}>}}
\newcommand{\todo}[1]{\textcolor{red}{#1}}


% --- Register some information about the document
% ---
\newcommand{\thelicencepic}{cc-by-sa.png}
\newcommand{\thelicencelink}{https://creativecommons.org}
\newcommand{\thelevel}{SNT}
\newcommand{\thesequence}{Séquence 0}
\newcommand{\themoreinNSI}{}

% --- Licence filler
\newcommand\licencepic{\includegraphics[height=\baselineskip]{../../licences/\thelicencepic}}


% --- More in NSI indicator
% --- This command just fill the variable themoreinNSI
\newcommand\moreinNSI{\def\themoreinNSI{Sujet approfondi en NSI}}

% --- Store title, author and date to use them later, even after maketitle emptyed them
% ---
\let\titleoriginal\title           % save original \title macro
\renewcommand{\title}[1]{          % substitute for a new \title
    \titleoriginal{#1}%               % define the real title
    \def\thetitle{#1}        % define \thetitle
}

\let\dateoriginal\date           % save original \date macro
\renewcommand{\date}[1]{          % substitute for a new \date
    \dateoriginal{#1}%               % define the real date
    \def\thedate{#1}        % define \thedate
}

\let\authororiginal\author           % save original \author macro
\renewcommand{\author}[1]{          % substitute for a new \author
    \authororiginal{#1}%               % define the real author
    \def\theauthor{#1}        % define \theauthor
}


% --- Header and footer
%---
\usepackage{fancyhdr}
\pagestyle{fancy}

\lhead{\themoreinNSI}
\chead{}
\rhead{\thelevel\thedate~: \thesequence}

\usepackage{lastpage}
\renewcommand{\footrulewidth}{0.4pt}% default hline is 0pt
\lfoot{\href{\thelicencelink}{\licencepic} \theauthor}
\cfoot{}
\rfoot{\thepage \,/ \pageref{LastPage}}


\def \ifempty#1{\def\temp{#1} \ifx\temp\empty }

% --- Document title
% ---
% remove spacing around date:
%\usepackage{titling}
%\predate{}
%\postdate{}
\AtBeginDocument{%
  \dateoriginal{} % clear date
  \authororiginal{}
  \ifx \@title\empty \else \maketitle \fi
  \thispagestyle{fancy}
}


% --- Academic year command
% --- Use: \academicyear to display text like "2020-2021"
% --- Use: \academicyear[-1] to display text like "2019-2020"
% --- Use: \academicyear[0][/][ to display text like "2020/2021"
\usepackage{xparse}
\newcounter{myyear}
\newcounter{myyearprevious}
\newcounter{myyearnext}
\NewDocumentCommand{\academicyear}{O{0} O{-}}{
    % Current year %
    \newcommand{\myyear}{\the\year}
    % Previous year %
    \newcommand\myyearprevious{\the\numexpr\myyear+#1-1\relax}
    % Next year %
    \newcommand\myyearnext{\the\numexpr\myyear+#1+1\relax}
    %% Print academic year %
    \ifnum\month<8 \myyearprevious#2\myyear \else\myyear#2\myyearnext\fi
}

% --- some optional packages
% \usepackage{multirow} % to use multirow in tabular
\usepackage{amsmath} % for more maths symbols and so on
% --- pour barrer du texte
\usepackage{ulem}

% --- Some color definitions
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\definecolor{rouge_brique}{HTML}{B6321C}
\xdefinecolor{darkblue}{rgb}{0,0,0.4}
\xdefinecolor{darkred}{rgb}{0.4,0,0}

% --- Javascript listing coloration
% ---
\lstdefinelanguage{JavaScript}{
  keywords={typeof, new, true, false, catch, function, return, null, catch, switch, var, if, in, while, do, else, case, break, let},
  keywordstyle=\color{blue}\bfseries,
  ndkeywords={class, export, boolean, throw, implements, import, this, document},
  ndkeywordstyle=\color{darkgray}\bfseries,
  identifierstyle=\color{black},
  sensitive=false,
  comment=[l]{//},
  morecomment=[s]{/*}{*/},
  commentstyle=\color{purple}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  morestring=[b]',
  morestring=[b]"
}

% --- Some Python highlightings presets
% ---
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    % keywordstyle=\color{magenta},
    keywordstyle=\color{blue},
    % keywordstyle=\color{darkblue},
    numberstyle=\tiny\color{codegray},
    % stringstyle=\color{codepurple},
    stringstyle=\color{rouge_brique},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2,
    inputencoding=utf8,
    extendedchars=true,
    literate={ç}{{\c{c}}}1 {à}{{`a}}1 {é}{{'e}}1 {è}{{`e}}1 {î}{{^i}}1,
}

% --- Some HTML highlightings presets
% ---
\lstdefinestyle{mystylehtml}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    % keywordstyle=\color{orange},
    keywordstyle=\color{blue},
    numberstyle=\tiny\color{codegray},
    % stringstyle=\color{codepurple},
    stringstyle=\color{red},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2
}

% --- Style configuration
\lstset{style=mystyle}
\lstset{language=Python}
\lstset{frame=lines}
\lstset{basicstyle=\footnotesize}


% --- Environment creator
% --- Handy to have a standard environment
% TODO add a symbole to each environment for daltonian students or B&W printing
\usepackage[many]{tcolorbox}

% This command define a new environment, cartouche style.
% 1: name
% 2: Text to display in cartouche
% 3: font color of cartouche
% 4: back color of cartouche
% TODO is there a way to reset the counter from a document?
\newcommand{\myenvironment}[4]{
  \newtcolorbox[
    auto counter,
%    number within=chapter
  ]{#1}{%
    %breakable, % Bug, add a cartouche on new page, over text
    enhanced,
    colback=white,
    colbacktitle=white,
    arc=0pt,
    boxrule=0pt,
    leftrule=1pt,
    rightrule=0pt,
    toprule=0pt,
    bottomrule=0pt,
    titlerule=0pt,
    colframe=#4,
    fonttitle=\color{#4}\sffamily\scshape,
    overlay={
      \node[
        outer sep=-1pt,
        anchor=west,
        fill=#4,
        font=\color{#3}\sffamily\scshape
      ] at (title.west) {#2~\thetcbcounter};
    },
    adjusted title={}
  }
}

% --- Some nice color sets
% ---
\definecolor{yellowtitle}{RGB}{133,100,4}
\definecolor{yellowtitleback}{RGB}{255,243,205}
\definecolor{yellowtitleborder}{RGB}{255,238,196}

\definecolor{turquoisetitle}{RGB}{12,84,96}
\definecolor{turquoisetitleback}{RGB}{209,236,241}
\definecolor{turquoisetitleborder}{RGB}{190,229,235}

\definecolor{redtitle}{RGB}{114,28,36}
\definecolor{redtitleback}{RGB}{248,215,218}
\definecolor{redtitleborder}{RGB}{245,198,203}


\definecolor{greentitle}{RGB}{21,87,36}
\definecolor{greentitleback}{RGB}{212,237,218}
\definecolor{greentitleborder}{RGB}{195,230,203}

% --- Some environments
% ---
%\myenvironment{exercice}{Exercice}{greentitle}{greentitleback}
\myenvironment{rappel}{Rappel}{greentitle}{greentitleback}
\myenvironment{notion}{Notions abordées}{greentitle}{greentitleback}
\myenvironment{example}{Exemple}{yellowtitle}{yellowtitleback}
\myenvironment{important}{Important}{yellowtitle}{yellowtitleback}
\myenvironment{further}{Pour aller plus loin}{greentitle}{greentitleback}
%\myenvironment{question}{Réponse - Question}{turquoisetitle}{turquoisetitleback}
\myenvironment{question}{Question}{turquoisetitle}{turquoisetitleback}
\myenvironment{definition}{Définition}{redtitle}{redtitleback}
\myenvironment{regle}{Règle}{redtitle}{redtitleback}
%\myenvironment{consigne}{Réponse - À faire}{redtitle}{redtitleback}
\myenvironment{consigne}{À faire}{redtitle}{redtitleback}
\myenvironment{savoir}{À savoir}{redtitle}{redtitleback}
\myenvironment{important2}{Important}{redtitle}{redtitleback}
\myenvironment{exercice}{Exercice}{turquoisetitle}{turquoisetitleback}
\myenvironment{methode}{Méthode}{turquoisetitle}{turquoisetitleback}



% --- Create fake \onslide and other commands for standalone picture
% ---
% TODO is this working?
\usepackage{xparse}
\NewDocumentCommand{\onslide}{s t+ d<>}{}
\NewDocumentCommand{\only}{d<>}{}
\NewDocumentCommand{\uncover}{d<>}{}
\NewDocumentCommand{\visible}{d<>}{}
\NewDocumentCommand{\invisible}{d<>}{}


% --- Some nice left bar for answer environment
% ---
\usepackage{framed}
\usepackage{xcolor}

\renewenvironment{leftbar}[1][\hsize]
{%
    \def\FrameCommand
    {%
        {\color{turquoisetitleborder}\vrule width 3pt}%
        \hspace{0pt}%must no space.
        \fboxsep=\FrameSep\colorbox{turquoisetitleback}%
    }%
    \MakeFramed{\hsize#1\advance\hsize-\width\FrameRestore}%
}
{\endMakeFramed}

% --- Answer environment
% --- Create an 'answer' environment, that will be hidden if the command \answersfalse is used
\usepackage{ifthen}
\newif\ifanswers
\answerstrue % Answers are displayed by default.

% Actual answer command
%\newenvironment{terseA}
%{
%    \ifanswers
%    \begin{itshape}
%    \leftbar
%}
%{
%    \endleftbar
%    \end{itshape}
%    \fi
%}
\newcommand\terseA[1]{
    \ifanswers
    \begin{itshape}
    \leftbar
    #1
    \endleftbar
    \end{itshape}
    \fi
}

% --- Question env
% --- Create a 'question' environment that display text 'Question' and a counter
\newcounter{simplequestions}
\setcounter{simplequestions}{1}
\newenvironment{terseQ}
{
    \textbf{Question \arabic{simplequestions}}
    \stepcounter{simplequestions}
}{
}
%\newcommand{\terseQuestion}[1]{
%    \textbf{Question \arabic{simplequestions}}
%    \stepcounter{simplequestions}
%    #1
%}

% --- A duration indication
% ---
\newcommand\duration[1]{
    \small{#1}
}


% --- Some default values
% ---
\date{\academicyear}
\title{}
\author{}
