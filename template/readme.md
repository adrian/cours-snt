# Template latex pour cours

## Fonctions inclues
- \bigO Notation en grand O 
- \ignore Son paramètre est ignoré
- \correc Colorie en vert
- \motcle Colorie en bleu
- \mothtml Colorie en bleau et entoure de chevrons <>
- \todo Colorie en rouge
- \academicyear Affiche l’année scolaire en cours
- \duration{1h} Affiche la durée avec une mise en forme fixe (utile dans les titres).

## Éléments de mise en page
- \def\thelicencepic{cc-by-sa.png} défini l’image du répertoire `licences` à utiliser comme photo pour la licence
- \def\thelicencelink{creativecommons.org} défini le lien vers lequel amener les utilisateurs ou utilisatrices qui cliquent sur la licence (n’est pas affiché à l’impression)
- \def\thelevel{SNT} défini le niveau d’enseignement
- \def\thesequence{Web} défini la séquence
- \moreinNSI Provoque l’affichage de l’encart spécifiant que le sujet est approfondi en NSI
- \title{} le titre
- \date{} la date
- \author{} l’auteur ou l’autrice

## Blocs numérotés
- rappel
- notion
- example
- important
- further
- {question
- question
- definition
- regle
- {consigne
- consigne
- savoir
- important2
- exercice
- methode

