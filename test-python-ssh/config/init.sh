#!/bin/bash
echo " ------------------------------ init.sh file ------------------------------"

# Allow SSH as root
if [ -z "$(grep '^PermitRootLogin yes' /etc/ssh/sshd_config)" ] ; then
    echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
fi

# set root passwd
echo -e "root\nroot" | passwd


echo " ------------------------------ init.sh end -------------------------------"
