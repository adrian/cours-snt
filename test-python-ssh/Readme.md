# Python, web and SSH sandbox
**For educational purpose only! None of this software is tested, optimized nor secured. It is actually unsecure on purpose**
This is a very experimental tool, **it may be working**. Any suggestion or PR is welcome.

# How to use it?
## Install docker
https://docs.docker.com/get-docker/

## Initialize it
The directory `config` must contain a `users.txt`, containing one username per line, or a `passwords.txt` file, containing `username=password` lines.  
If you do not provide a password file, it will be generated from user file.  
The password file is the database from which users/passwords are created in the system.  
Aditionnaly, you can add a file named `./config/init.sh` which will be executed (as root) before starting the servers. It is usefull for debuging and customisation purposes!

## Run it
While in this file directory, open a terminal and run the following command (the download can be very long…).
```
docker run -it --name pythonsandbox --rm --network host -v "$(pwd)"/production_eleves:/app/python_app/modules -v "$(pwd)"/config:/app/config jeancloud/pythonsandbox
```
(Logs will flow in your terminal, CTRL+C will stop the process).  
Read carefuly the first messages!


## Use it
You can now ssh into your localhost (and others computer on the same network can ssh into your host).
Usernames and passwords are the one provided in the password file.  
Students home directories are then listed in the `production_eleves` directory.

## Debug it
You can open a shell (as root) in the container anytime by running this command on the docker host:
```
docker exec -it pythonsandbox bash
```

# The docker image
To bundle everything in one place.
This docker image is not a pretty one, we should split those services into several containers.
But that would be harder to run, so forget that.  
Also, as this is poorly tested, the docker system make sure the environment is stable.

## Build the docker image
```
docker build . -t jeancloud/pythonsandbox
```
or pull it
```
docker pull jeancloud/pythonsandbox
```

## Volumes
- `/app/modules` is where python scripts will be executed on some URL calls.
- `/app/config` is the location of the user, password and init files.

## Environment variables
None used, do watever you want

# How does it works?

## A python script
It run with uwsgi (CF dockerfile CMD line) and load python modules files according to URL.
For instance, when you HTTP-get `/test/my_function` the function `my_function` is executed from the file `test.py`.  
###Default behavior:
- if `test` is a directory, we will try to load default function `index` from file `test/my_function.py`
- if `test/my_function` is a directory, we will try to load default function `index` from file `test/my_function/main.py`
If you don’t like this default behavior, just don’t use main and index names.
### Arguments
GET arguments (the ones in URL), are passed as function parameter (only if the parameters name matches the arguments name).

## SSH server
Allow student to connect via SSH or SFTP to add python files and play with bash.

## NGINX HTTP server
For more flexibility with HTTP

# TODOs
- public directory in homes where content is served
