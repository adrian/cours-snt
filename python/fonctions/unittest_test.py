
def bonjour ():
    """ Cette fonction affiche le texte « Bonjour à tous » """
    print('Bonjour à tous')

def somme (a, b):
    """ Cette fonction retourne la somme de a et b """
    return a+b

# Exercice 1 — Écrivez une fonction nommée « trois » qui retourne la valeur 3
# Exercice 2 — Écrivez une fonction nommée « double » qui prend un nombre en paramètre et retourne le double de sa valeur
# Exercice 3 — Écrivez une fonction nommée « triple » qui prend un nombre en paramètre et retourne le double de sa valeur
# Exercice 4 — Écrivez une fonction nommée « carre » qui prend un nombre en paramètre et retourne le carré de sa valeur
# Exercice 5 — Écrivez une fonction nommée « est_majeur » qui prend un âge en paramètre et retourne True si cet âge est plus grand que 18, False sinon
# Exercice 6 — Écrivez une fonction nommée « est_grand » qui prend une taille en centimètres en paramètre et retourne True si elle est plus grande que 160, False sinon
# Exercice 7 — En vous aidant des fonctions est_majeur et est_grand, écrivez une fonction nommée « peut_faire_le_manege » qui prend un âge et une taille en paramètre et renvoie true si la taille est plus grande que 160 et que l’âge est plus grand que 18


if __name__ == '__main__':
    import unittest
    class MyTests(unittest.TestCase):
        # return True or False
        def test_trois (self):
            self.assertEqual(trois(), 3)
        def test_double (self):
            self.assertEqual(double(0), 0)
            self.assertEqual(double(-11), -22)
            self.assertEqual(double(7), 14)
        def test_carre (self):
            self.assertEqual(carre(0), 0)
            self.assertEqual(carre(-11), 121)
            self.assertEqual(carre(7), 49)
        def test_est_majeur (self):
            self.assertFalse(est_majeur(0))
            self.assertFalse(est_majeur(-11))
            self.assertTrue(est_majeur(18))
            self.assertTrue(est_majeur(30))
        def test_est_grand (self):
            self.assertFalse(est_grand(0))
            self.assertFalse(est_grand(-11))
            self.assertTrue(est_grand(180))
            self.assertTrue(est_grand(160))
        def test_peut_faire_le_manege (self):
            self.assertFalse(peut_faire_le_manege(0, 0))
            self.assertFalse(peut_faire_le_manege(30, 150))
            self.assertFalse(peut_faire_le_manege(17, 180))
            self.assertTrue(peut_faire_le_manege(20, 180))
    unittest.main()
