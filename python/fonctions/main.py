
def bonjour ():
    """ Cette fonction affiche le texte « Bonjour à tous » """
    print('Bonjour à tous')

def somme (a, b):
    """ Cette fonction retourne la somme de a et b """
    return a+b

# Exercice 1 — Écrivez une fonction nommée « trois » qui retourne la valeur 3
# Exercice 2 — Écrivez une fonction nommée « double » qui prend un nombre en paramètre et retourne le double de sa valeur
# Exercice 3 — Écrivez une fonction nommée « triple » qui prend un nombre en paramètre et retourne le triple de sa valeur
# Exercice 4 — Écrivez une fonction nommée « carre » qui prend un nombre en paramètre et retourne le carré de sa valeur
# Exercice 5 — Écrivez une fonction nommée « est_majeur » qui prend un âge en paramètre et retourne True si cet âge est plus grand que 18, False sinon
# Exercice 6 — Écrivez une fonction nommée « est_grand » qui prend une taille en centimètres en paramètre et retourne True si elle est plus grande que 160, False sinon
# Exercice 7 — En vous aidant des fonctions est_majeur et est_grand, écrivez une fonction nommée « peut_faire_le_manege » qui prend un âge et une taille en paramètre et renvoie true si la taille est plus grande que 160 et que l’âge est plus grand que 18



if __name__ == '__main__':
    def get_function (name):
        try:
            return getattr(m, 'sommet')
        except AttributeError:
            print('************************* ERREUR *************************')
            print('La fonction « {} » n’a pas été trouvée'.format(name))
    def test (function_name, test_function):
        f = get_function(function_name)

    import importlib, os
    m = importlib.import_module(os.path.basename(__file__)[0:-3])



