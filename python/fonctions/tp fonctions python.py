# Voici Deux exemples de fonctions

def bonjour ():
    """ Cette fonction affiche le texte « Bonjour à tous » """
    print('Bonjour à tous')

def somme (a, b):
    """ Cette fonction retourne la somme de a et b """
    return a+b


# Exercice 1 — Écrivez une fonction nommée « trois » qui retourne la valeur 3

# Exercice 2 — Écrivez une fonction nommée « double » qui prend un nombre en paramètre et retourne le double de sa valeur

# Exercice 3 — Écrivez une fonction nommée « triple » qui prend un nombre en paramètre et retourne le triple de sa valeur

# Exercice 4 — Écrivez une fonction nommée « carre » qui prend un nombre en paramètre et retourne le carré de sa valeur

# Exercice 5 — Écrivez une fonction nommée « est_majeur » qui prend un âge en paramètre et retourne True si cet âge est plus grand que 18, False sinon

# Exercice 6 — Écrivez une fonction nommée « est_grand » qui prend une taille en centimètres en paramètre et retourne True si elle est plus grande que 140, False sinon

# Exercice 7 - En vous aidant de la fonction « est_majeur », écrivez une fonction nommée « est_mineur » qui prend un âge en paramètre et renvoie True si l’âge est strictement inférieur à 18

# Exercice 8 — En vous aidant des fonctions est_majeur et est_grand, écrivez une fonction nommée « peut_faire_le_manege » qui prend un âge et une taille en paramètre et renvoie true si la taille est plus grande que 140 et que l’âge est plus grand que 18

# Exercice 9 — En vous aidant des fonctions est_majeur et est_grand, écrivez une fonction nommée « est_un_enfant » qui prend un âge et une taille en paramètre et renvoie true si la taille est plus petite que 140 et que l’âge est plus petit que 18

# Exercice 10 — En vous aidant des fonctions double et est_grand, écrivez une fonction nommée « agrandir » qui prend une taille en paramètre et renvoie la taille doublée si elle est plus petite que 140 mais la renvoie non modifiée sinon






##################################################################################################
#   Le code python suivant permet de faire les tests, n’écrivez rien en dessous de cette ligne   #
##################################################################################################

if __name__ == '__main__':
    import importlib, os
    tests = [
        {
            'function_name': 'trois',
            'tests': [
                {'params': [], 'result': 3},
            ],
        },
        {
            'function_name': 'double',
            'tests': [
                {'params': [2], 'result': 4},
                {'params': [0], 'result': 0},
                {'params': [-12], 'result': -24},
            ]
        },
        {
            'function_name': 'triple',
            'tests': [
                {'params': [2], 'result': 6},
                {'params': [0], 'result': 0},
                {'params': [-12], 'result': -36},
            ]
        },
        {
            'function_name': 'carre',
            'tests': [
                {'params': [4], 'result': 16},
                {'params': [0], 'result': 0},
                {'params': [-12], 'result': 144},
            ]
        },
        {
            'function_name': 'est_majeur',
            'tests': [
                {'params': [4], 'result': False},
                {'params': [18], 'result': True},
                {'params': [76], 'result': True},
                {'params': [-12], 'result': False},
            ]
        },
        {
            'function_name': 'est_grand',
            'tests': [
                {'params': [4], 'result': False},
                {'params': [140], 'result': True},
                {'params': [200], 'result': True},
                {'params': [-12], 'result': False},
            ],
        },
        {
            'function_name':'peut_faire_le_manege',
            'tests': [
                {'params': [18, 140], 'result': True},
                {'params': [2, 140], 'result': False},
                {'params': [18, 100], 'result': False},
                {'params': [-2, -1], 'result': False},
            ]
        },
        {
            'function_name':'est_un_enfant',
            'tests': [
                {'params': [18, 140], 'result': False},
                {'params': [2, 140], 'result': False},
                {'params': [18, 100], 'result': False},
                {'params': [-2, -1], 'result': True},
            ]
        },
        {
            'function_name':'agrandir',
            'tests': [
                {'params': [4], 'result': 8},
                {'params': [-8], 'result': -16},
                {'params': [140], 'result': 140},
                {'params': [150], 'result': 150},
            ]
        },
    ]
    print('\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n')
    print('------------------------------- Restart --------------------------------')
    print('\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n')
    nb_tests = sum(len(item['tests']) for item in tests)
    nb_ok = 0
    num_exercice = 0
    try: # Import the module
        m = importlib.import_module(os.path.basename(__file__)[0:-3])
    except ModuleNotFoundError:
        print('Le fichier module n’a pas été trouvé.')
        exit(1)

    for item in tests:
        num_exercice += 1
        print('\n---------------------------- Exercice {} ---------------------------- \n'.format(num_exercice))
        # Les dictionnaires ne sont pas ordonnés sur certaines versions de python
        #print('\n---------------------------------------------------------------------- \n')
        try:
            f = getattr(m,item['function_name'])
        except AttributeError:
            print('************************* ATTENTION *************************')
            print('La fonction « {} » n’a pas été trouvée'.format(item['function_name']))
            continue

        print('On va tester la fonction {}'.format(item['function_name']))
        for jeu in item.tests[item['function_name']]:
            print('On exécute {}({})'.format(item['function_name'],', '.join((str(i) for i in jeu['params']))))
            try:
                result = f(*jeu['params'])
                print('-> Résultat : ' + str(result))
                if result == jeu['result']:
                    print('OK')
                    nb_ok += 1
                else:
                    print('************************* RÉSULTAT INATTENDU *************************')
                    print('On attendait le résultat suivant : ' + str(jeu['result']))
            except TypeError:
                print('************************* PROBLÈME DE PARAMÈTRES *************************')
                print('TypeError : Le nombre de paramètres de la fonction « {} » est-il bon ?'.format(function_name))
                break
    print('\nTotal des tests : {}/{}'.format(nb_ok, nb_tests))
