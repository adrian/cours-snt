from turtle import *
from random import random

# L’idée est ici de faire une fonction que les élève pourraient comprendre
def carre(longueur, angle=0):
    """Cette fonction trace un carré de centre 0,0 de longueure et d’inclinaison variables"""
    up() # Ces 3 lignes placent la tortue au centre du dessin
    goto(0,0)
    setheading(angle)
    forward(longueur/2) # Ces 3 lignes placent la tortue dans un angle de notre futur carré
    left(90)
    forward(longueur/2)
    down()
    for i in range(4): # Ces 3 lignes tracent un carré
        left(90)
        forward(longueur)

def partie1():
    nombre_fleurs = 10
    for f in range(nombre_fleurs):
        for i in range(10):
            carre(100 + 50*f, i*10)

def partie2():
    while True:
        for i in range(50):
            pencolor(random(), random(), random())
            carre(50 + i*10, i*10)
        for i in range(50,0,-1):
            pencolor(random(), random(), random())
            carre(50 + i*10, i*10)

speed(0)
hideturtle()
partie2()
