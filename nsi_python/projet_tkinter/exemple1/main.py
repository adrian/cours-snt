from tkinter import *

###############################################################################
# Fonction de navigation entre les fenêtres
###############################################################################

def afficher_fenetre1():
    racine.withdraw() # cacher la fenetre racine
    fenetre1.deiconify() # afficher la fenetre1

def afficher_racine():
    fenetre1.withdraw() # cacher la fenetre racine
    racine.deiconify() # afficher la fenetre1


###############################################################################
# Définitions de la fenêtre racine
###############################################################################

# La fenêtre racine. Il ne peut y en avoir qu’une.
racine = Tk()

# On met un label sur la racine pour identifier la fenêtre.
label1 = Label(racine, text="""Ceci est la fenêtre racine, l’application se termine lorsqu’on la ferme.""")
label1.pack()

# On a un bouton qui ouvre une nouvelle fenetre
button1 = Button(racine, text="afficher", command=afficher_fenetre1)
button1.pack()

###############################################################################
# Définitions de la fenêtre 1
###############################################################################

# On peut créer autant de fenêtres toplevel que l’on veut
fenetre1 = Toplevel(racine)
label2 = Label(fenetre1, text="""Ceci est une fenêtre en plus, la fermer n’affectera pas les autres.""")
label2.pack()

# Un bouton pour quiter le programme. Il appelle la fonction racine.quit()
bouton_quiter = Button(fenetre1, text="quiter", command=racine.quit)
bouton_quiter.pack()

# Un bouton qui revient à la fenetre racine
bouton_racine = Button(fenetre1, text="Retour", command=afficher_racine)
bouton_racine.pack()

# On cache la fenetre1
fenetre1.withdraw()


###############################################################################
# Démarrage du programme
###############################################################################

racine.mainloop()

