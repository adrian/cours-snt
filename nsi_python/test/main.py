def pair(n):
    if n % 2 == 0:
        return True
    else:
        return False

def tests():
    assert pair(0) == True
    assert pair(-1) == False
    assert pair(2) == True
    print('Tous les tests ont été fait')

tests()
